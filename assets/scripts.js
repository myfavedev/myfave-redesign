function myFunction() {
  document.getElementById("createDropdown").classList.toggle("show-dropdown");
}

function userProfile() {
  document.getElementById("userDropdown").classList.toggle("show-dropdown");
  console.log(document.getElementById("userDropdown"))
}

function accountProfile() {
  document.getElementById("accountDropdown").classList.toggle("show-dropdown");
}

function boardsMenu() {
  document.getElementById("boardsDropdown").classList.toggle("show-dropdown");
}

function requestfeature() {
	document.getElementById("requestfeature").classList.toggle("request-feature-active");
}

function logoMenu() {
	document.getElementById("logo-menu").classList.toggle("logo-menu-active");
}

function authenticationEnable() {
  document.getElementById("authenticationDropdown").classList.toggle("show-menu");
}

function quickEditCard(event) {
	const element = document.getElementsByClassName("quick-card-editor")[0]
	element.classList.toggle("active-editor");
	const inputElement = document.getElementsByClassName("quick-card-editor-card")[0];
	const top = event.clientY - 25;
	const left = event.clientX - 230;
	console.log(event.clientX)
	inputElement.style.top = `${top}px`;
	inputElement.style.left = `${left}px`;
}

function closeCard(event) {
	const element = document.getElementsByClassName("quick-card-editor")[0]
	element.classList.remove("active-editor");
}

function openModalWindow(event) {
  if (!event.target.matches('.fa-pencil')) {
    const element = document.getElementById("window-overlay");
    element.style.display = 'flex';
  }
}

function privateDropdown() {
  document.getElementById("privateDropdown").classList.toggle("show-dropdown");
}

function closeModalWindow(event) {
  const element = document.getElementById("window-overlay");
  element.style.display = 'none';
}

function openProfileModal(event) {
  if (!event.target.matches('.fa-pencil')) {
    const element = document.getElementById("profile-overlay");
    element.style.display = 'flex';
  }
}

function closeProfileWindow(event) {
  const element = document.getElementById("profile-overlay");
  element.style.display = 'none';
}


// Close the dropdown menu if the user clicks outside of it
// window.onclick = function(event) {
//   if (!event.target.matches('.dropdown-content')) {

//     var dropdowns = document.getElementsByClassName("dropdown-content");
//     var i;
//     for (i = 0; i < dropdowns.length; i++) {
//       var openDropdown = dropdowns[i];
//       if (openDropdown.classList.contains('show-dropdown')) {
//         openDropdown.classList.remove('show-dropdown');
//       }
//     }
//   }
// }






